---
title: "Linux Notes"
date: 2023-02-10T21:34:45-06:00
draft: false
---

### Find in files 

### Find Files

find . -name *foo                 # find files with foo recursivley
find . -print | grep -i foo*      # Case insensitive

f() { find . -name "*$1*"; }


### Linux Printer
lpadmin -p <unique-name> -E -v ipp://<ip address>/ipp/print -m everywhere

### Linux Screen Capture / Video
<p> 
``` CTRL + SHIFT + ALT + P  ```  start and stop video
Set video to unlimited timeout.
```
gsettings set org.gnome.settings-daemon.plugins.media-keys max-screencast-length 0
``` 
<p>


# MAC
Remove quarantine of files
xattr -f com.apple.quarantine /path/to/file

