---
title: "Git Notes"
date: 2023-02-10T21:34:45-06:00
draft: false
---
### Stash
```
git stash 
git stash list
git stash apply stash{number}
```


### Show Logs
```
git log --graph
git log --graph --pretty=oneline
```

### Branches 
```
git checkout -b <branch name>
git push origin <branch name>
git push --set-upstream origin <branch name>

```
### Stream
```
git remote -v   # list remote
git remote set-url origin <path to git repo>   # set the remote.
```